// Initialize the Amazon Cognito credentials provider
AWS.config.region = 'us-east-1'; // Region
AWS.config.credentials = new AWS.CognitoIdentityCredentials({
    IdentityPoolId: 'us-east-1:195a5668-dd67-499b-a737-84ee728915fb',
});
 var kinesisResponse = window._kinesisResponse;

const kinesisClient = new AWS.Kinesis();
const KDS_STREAM_NAME = "AmazonRekognition-kinesis-stream-webcam-demo-kds";

function initKinesisPolling() {
    var DEFAULT_RECORDS_LIMIT = 20; // max records to retrieve per request
    var POLLING_INTERVAL = 3000; // milliseconds
    var SHARD_ID = 'shardId-000000000000'; // app publishes to only one shard
    var shardIterator;
    var getNextShard = function () {
        kinesisClient.getRecords({
            Limit: DEFAULT_RECORDS_LIMIT,
            ShardIterator: shardIterator
        }, defaultCallback);
    };
    var defaultCallback = function (err, data) {
        if (err) return console.error(err);
        shardIterator = data.NextShardIterator || data.ShardIterator;
        if (data.Records) {
            data.Records.forEach((record) => {
                const payload = record.Data.toString('ascii');
                const kinesisData = JSON.parse(payload);
                console.log(kinesisData);
                setResponse(kinesisData);
                var emotionsData = [];
                kinesisData.Emotions.forEach((emotion) => { 
                    emotionsData.push(emotion.Confidence)
                })
                generateChart(emotionsData);
            });
            setTimeout(function () {
                getNextShard();
            }, POLLING_INTERVAL)
        } else {
            getNextShard();
        }
    };
    kinesisClient.getShardIterator({
        ShardIteratorType: 'LATEST',
        StreamName: KDS_STREAM_NAME,
        ShardId: SHARD_ID
    }, defaultCallback);
}

function setResponse(resp){
    kinesisResponse = resp;
}