const DATA_ENDPOINT = "https://2mjly7nov1.execute-api.us-east-1.amazonaws.com/Prod/convert"
// const DATA_ENDPOINT = "http://127.0.0.1:3000/hello";

Webcam.set({
    width: 640,
    height: 480,
    image_format: "jpeg"
});

// Attempt to stream webcam feed to canvas element.
Webcam.attach("#my_camera");
// When webcam feed acquired, executes callback.
Webcam.on('live', () => {
    startStreamLoop();
    initKinesisPolling();
});

var looperPromise;
function startStreamLoop() {
    var looper = function () {
        // Pass current frame image data to handler.
        Webcam.snap((dataUri) => {
            upload2S3(dataUri);
        });
        looperPromise = setTimeout(looper, 3000);
    }
    looper();
}

function uploadObject(data, endpoint, callback) {
    var $http = new XMLHttpRequest();
    $http.open("POST", endpoint);
    $http.setRequestHeader("Content-Type", "application/json");
    $http.send(JSON.stringify(data));
}

function postFrameData(data, endpoint, callback) {
    var $http = new XMLHttpRequest();
    $http.open("POST", endpoint);
    $http.setRequestHeader("Content-Type", "application/json");
    $http.send(JSON.stringify(data));
}

function stopStreaming() {
    try {
        Webcam.reset();
        Webcam.off('live');
        clearTimeout(looperPromise);
        frameBuffer.clear();
    } catch (e) {
        console.error(e);
    }
}
