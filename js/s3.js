const s3Client = new AWS.S3();

function upload2S3(imageFile) {
    fetch(imageFile)
        .then(res => res.blob())
        .then(blob => {
            const file = new File([blob], new Date().getTime() + ".jpeg");
            var params = {
                Body: file,
                Bucket: "kinesis-stream-webcam-demo-uploads-560752137933",
                Key: new Date().getTime() + ".jpeg",
                ContentType: "image/jpeg",
                Metadata: {}
            };
            s3Client.putObject(params, function (err, data) {
                if (err) console.log(err, err.stack); // an error occurred
                else console.log(data);           // successful response
            });
        })
    
}